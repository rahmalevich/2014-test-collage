//
//  TCRenderingHelper.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 14.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCRenderingHelper.h"

@implementation TCRenderingHelper

+ (UIImage *)placeholderImageForSize:(CGSize)size
{
    CGRect placeholderRect = (CGRect){CGPointZero, size};
    
    CGFloat dashPattern[] = {5, 3};
    UIBezierPath *placeholderPath = [UIBezierPath bezierPathWithRoundedRect:placeholderRect cornerRadius:5.0];
    placeholderPath.lineWidth = 2.0;
    [placeholderPath setLineDash:dashPattern count:2 phase:0];
    
    UIGraphicsBeginImageContext(placeholderRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    [placeholderPath stroke];
    UIImage *placeholderImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return placeholderImage;
}


@end
