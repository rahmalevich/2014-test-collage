//
//  TCStartViewController.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 04.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCStartViewController.h"
#import "TCCollageViewController.h"

@interface TCStartViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nickTextField;
@end

@implementation TCStartViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Введите имя пользователя";
}

#pragma mark - Actions
- (IBAction)startAction:(UIButton *)sender
{
    NSString *username = _nickTextField.text;
    if ([username length] > 0) {
        TCCollageViewController *collageController = [[TCCollageViewController alloc] initWithUsername:username];
        [self.navigationController pushViewController:collageController animated:YES];
    }
}

@end
