//
//  TCPhotoObject.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 10.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCPhotoObject.h"

@interface TCPhotoObject ()

@property (nonatomic, assign) NSInteger likesCount;
@property (nonatomic, copy, readwrite) NSString *thumbnail;
@property (nonatomic, copy, readwrite) NSString *lowResolution;
@property (nonatomic, copy, readwrite) NSString *standardResolution;

@end

@implementation TCPhotoObject

+ (NSArray *)photosFromArray:(NSArray *)array
{
    NSMutableArray *resultArray = [NSMutableArray array];
    for (NSDictionary *photoDict in array) {
        if ([photoDict isKindOfClass:[NSDictionary class]]) {
            TCPhotoObject *photoObject = [[TCPhotoObject alloc] initWithData:photoDict];
            [resultArray addObject:photoObject];
        }
    }
    return resultArray;
}

- (instancetype)initWithData:(NSDictionary *)photoData
{
    if (self = [super init]) {
        self.thumbnail = [photoData valueForKeyPath:@"images.thumbnail.url"];
        self.lowResolution = [photoData valueForKeyPath:@"images.low_resolution.url"];
        self.standardResolution = [photoData valueForKeyPath:@"images.standard_resolution.url"];
        self.likesCount = [[photoData valueForKeyPath:@"likes.count"] integerValue];
    }
    return self;
}

@end
