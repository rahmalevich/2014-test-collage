//
//  TCNetworkHelper.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 10.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCNetworkHelper.h"
#import "AFNetworking.h"

static NSString *const kInstagramClientId = @"8dcf1f57075c433ba0f284e841ecd245";
static NSString *const kBaseURL = @"https://api.instagram.com";

@interface TCNetworkHelper ()
@property (nonatomic, strong) AFHTTPRequestOperationManager *manager;
@end

@implementation TCNetworkHelper

static TCNetworkHelper *_sharedInstance = nil;
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [TCNetworkHelper new];
        _sharedInstance.manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
        _sharedInstance.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    });
    return _sharedInstance;
}

- (void)loadRequestWithPath:(NSString *)path params:(NSDictionary *)params callback:(TCNetworkResponseBlock)callback
{
    [callback copy];
    
    NSMutableDictionary *mutableParams = [NSMutableDictionary dictionaryWithDictionary:params];
    mutableParams[@"client_id"] = kInstagramClientId;
    [_manager GET:path parameters:mutableParams success:^(AFHTTPRequestOperation *operation, id responseObject){
        callback(responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        callback(nil, error);
    }];
}


@end
