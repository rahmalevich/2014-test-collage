//
//  TCDataController.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 09.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

@class TCDataController;
@protocol TCDataControllerDelegate <NSObject>
- (void)dataControllerWillStartLoading:(TCDataController *)controller;
- (void)dataControllerDidFinishLoading:(TCDataController *)controller;
- (void)dataController:(TCDataController *)controller loadingDidFailWithError:(NSError *)error;
@end

@interface TCDataController : NSObject

@property (nonatomic, copy, readonly) NSString *username;
@property (nonatomic, strong, readonly) NSArray *photos;

- (instancetype)initWithUsername:(NSString *)username delegate:(id<TCDataControllerDelegate>)delegate;
- (void)loadPhotos;

@end
