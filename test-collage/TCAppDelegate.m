//
//  TCAppDelegate.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 04.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCAppDelegate.h"
#import "TCStartViewController.h"

@implementation TCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    
    TCStartViewController *startController = [TCStartViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:startController];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

@end
