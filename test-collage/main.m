//
//  main.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 04.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TCAppDelegate class]));
    }
}
