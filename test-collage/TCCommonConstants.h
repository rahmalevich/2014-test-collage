//
//  TCCommonConstants.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 11.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

static NSInteger const kBestPhotosCount = 20;

static CGFloat const kCollageSideSize = 400.0f;

static NSString *const kTCErrorDomain = @"kTCErrorDomain";

typedef enum {
    TCErrorCodeUserNotFound = -100
} TCErrorCode;
