//
//  TCPhotoObject.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 10.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

@interface TCPhotoObject : NSObject

@property (nonatomic, readonly) NSInteger likesCount;
@property (nonatomic, copy, readonly) NSString *thumbnail;
@property (nonatomic, copy, readonly) NSString *lowResolution;
@property (nonatomic, copy, readonly) NSString *standardResolution;

+ (NSArray *)photosFromArray:(NSArray *)array;
- (instancetype)initWithData:(NSDictionary *)photoData;

@end
