//
//  TCPickerViewController.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 04.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCPickerViewController.h"
#import "TCPickerViewCell.h"
#import "TCDataController.h"
#import "TCPhotoObject.h"

static NSString* kPickerCellReuseId = @"kPickerCellReuseId";

@interface TCCollectionViewFlowLayout : UICollectionViewFlowLayout
- (CGSize)collectionViewContentSize;
@end

@implementation TCCollectionViewFlowLayout

- (CGSize)collectionViewContentSize
{
    CGSize size = [super collectionViewContentSize];
    return CGSizeMake(size.width, MAX(size.height, self.collectionView.bounds.size.height + 1));
}

@end

@interface TCPickerViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, copy) TCDataBlock callback;
@property (nonatomic, strong) TCDataController *dataController;
@property (nonatomic, strong) NSArray *selectedPhotos;
@end

@implementation TCPickerViewController

#pragma mark - Initialization
- (instancetype)initWithDataController:(TCDataController *)dataController selectedPhotos:(NSArray *)selectedPhotos callback:(TCDataBlock)callback
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        self.callback = callback;
        self.dataController = dataController;
        self.selectedPhotos = selectedPhotos;
    }
    return self;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"TCPickerViewCell" bundle:nil] forCellWithReuseIdentifier:kPickerCellReuseId];
}

#pragma mark - Actions
- (IBAction)actionCancel:(UIBarButtonItem *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionView delegate & datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_dataController.photos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TCPickerViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPickerCellReuseId forIndexPath:indexPath];
    TCPhotoObject *photo = _dataController.photos[indexPath.row];
    BOOL isAlreadyPicked = [_selectedPhotos containsObject:photo];
    [cell setupWithPhoto:photo isAlreadyPicked:isAlreadyPicked];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TCPhotoObject *photo = _dataController.photos[indexPath.row];
    BOOL isAlreadyPicked = [_selectedPhotos containsObject:photo];
    if (!isAlreadyPicked) {
        if (_callback) {
            _callback(photo);
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end