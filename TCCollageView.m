//
//  TCCollageView.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 11.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCCollageView.h"
#import "TCCollage.h"
#import "TCRenderingHelper.h"
#import "TCPhotoObject.h"
#import "UIImageView+WebCache.h"

@interface TCCollageView ()
@property (nonatomic, weak) id<TCCollageViewDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *imageViewsArray;
@property (nonatomic, strong) TCCollage *collage;
@end

@implementation TCCollageView

#pragma mark - Initialization
- (instancetype)initWithFrame:(CGRect)frame collage:(TCCollage *)collage delegate:(id<TCCollageViewDelegate>)delegate
{
    if (self = [super initWithFrame:frame]) {
        self.collage = collage;
        self.delegate = delegate;
        self.imageViewsArray = [NSMutableArray array];
        [self createViewHierarchy];
    }
    return self;
}

- (void)createViewHierarchy
{
    self.backgroundColor = [UIColor lightGrayColor];
    
    NSArray *cellRectsArray = [_collage photoCellsFramesForCollageWithSize:self.bounds.size];
    for (NSInteger i = 0; i < [cellRectsArray count]; i++) {
        CGRect rect = [cellRectsArray[i] CGRectValue];
        TCPhotoObject *photo = [_collage photoAtIndex:i];
        [self addPhotoCellWithFrame:rect photo:photo];
    }
}

- (void)addPhotoCellWithFrame:(CGRect)frame photo:(TCPhotoObject *)photo
{
    UIImageView *photoImageView = [[UIImageView alloc] initWithFrame:frame];
    [self setPhoto:photo toImageView:photoImageView];
    [_imageViewsArray addObject:photoImageView];
    [self addSubview:photoImageView];
    
    UIButton *repickPhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    repickPhotoButton.frame = frame;
    repickPhotoButton.tag = [_imageViewsArray count] - 1;
    [repickPhotoButton addTarget:self action:@selector(actionPickPhoto:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:repickPhotoButton];
}

- (void)setPhoto:(TCPhotoObject *)photo toImageView:(UIImageView *)imageView
{
    UIImage *placeholderImage = [TCRenderingHelper placeholderImageForSize:imageView.bounds.size];
    [imageView setImageWithURL:[NSURL URLWithString:photo.lowResolution] placeholderImage:placeholderImage];
}

#pragma mark - Public
- (void)updatePhotos
{
    for (NSInteger i = 0; i < [_collage photoCellsCount]; i++) {
        UIImageView *imageView = (i < [_imageViewsArray count]) ? _imageViewsArray[i] : nil;
        if (imageView) {
            TCPhotoObject *photo = [_collage photoAtIndex:i];
            [self setPhoto:photo toImageView:imageView];
        }
    }
}

#pragma mark - Actions
- (void)actionPickPhoto:(UIButton *)button
{
    [_delegate didTapPhotoWithIndex:button.tag atCollageView:self];
}

@end
