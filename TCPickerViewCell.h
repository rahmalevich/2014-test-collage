//
//  TCPickerViewCell.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 11.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

@class TCPhotoObject;
@interface TCPickerViewCell : UICollectionViewCell

- (void)setupWithPhoto:(TCPhotoObject *)photo isAlreadyPicked:(BOOL)isAlreadyPicked;

@end
