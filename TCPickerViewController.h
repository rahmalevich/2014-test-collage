//
//  TCPickerViewController.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 10.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

@class TCDataController;
@interface TCPickerViewController : UIViewController

- (instancetype)initWithDataController:(TCDataController *)dataController selectedPhotos:(NSArray *)selectedPhotos callback:(TCDataBlock)callback;

@end
