//
//  TCPickerViewCell.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 11.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCPickerViewCell.h"
#import "TCPhotoObject.h"
#import "UIImageView+WebCache.h"

@interface TCPickerViewCell ()
@property (weak, nonatomic) IBOutlet UIView *selectionView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (nonatomic, assign) BOOL isAlreadyPicked;
@end

@implementation TCPickerViewCell

#pragma mark - Initialization
- (void)setupWithPhoto:(TCPhotoObject *)photo isAlreadyPicked:(BOOL)isAlreadyPicked
{
    self.isAlreadyPicked = isAlreadyPicked;
    _selectionView.hidden = !isAlreadyPicked;
    [self.photoImageView setImageWithURL:[NSURL URLWithString:photo.thumbnail]];
}

#pragma mark - View lifecycle
- (void)setHighlighted:(BOOL)highlighted
{
    if (!_isAlreadyPicked) {
        [super setHighlighted:highlighted];
        _selectionView.hidden = !highlighted;
    }
}

@end

