//
//  TCNetworkHelper.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 10.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

@interface TCNetworkHelper : NSObject

+ (instancetype)sharedInstance;
- (void)loadRequestWithPath:(NSString *)path params:(NSDictionary *)params callback:(TCNetworkResponseBlock)callback;

@end
