//
//  TCCollageViewController.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 04.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

@interface TCCollageViewController : UIViewController

- (instancetype)initWithUsername:(NSString *)username;

@end
