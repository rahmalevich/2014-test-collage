//
//  TCCollage.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 11.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCCollage.h"
#import "TCCollageView.h"
#import "TCPhotoObject.h"
#import "TCRenderingHelper.h"
#import "SDWebImagePrefetcher.h"
#import "SDImageCache.h"

@interface TCCollage ()
@property (nonatomic, strong) NSMutableDictionary *photosDictionary;
@property (nonatomic, strong) TCCollageView *collageView;
@end

@implementation TCCollage

#pragma mark - Initialization
- (instancetype)initWithPhotos:(NSArray *)photos
{
    if (self = [super init]) {
        self.photosDictionary = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark - Photo methods
- (NSInteger)photoCellsCount
{
    // override in subclasses
    return 0;
}

- (NSArray *)photoCellsFramesForCollageWithSize:(CGSize)size
{
    // override in subclasses
    return nil;
}

- (NSArray *)allPhotos
{
    return [_photosDictionary allValues];
}

- (void)setPhoto:(TCPhotoObject *)photo atIndex:(NSInteger)index
{
    if (photo && index < [self photoCellsCount]) {
        [_photosDictionary setObject:photo forKey:@(index)];
    }
}

- (TCPhotoObject *)photoAtIndex:(NSInteger)index
{
    TCPhotoObject *photo =[_photosDictionary objectForKey:@(index)];
    return photo;
}

#pragma mark - Rendering
- (void)saveToImageWithCallback:(TCDataBlock)callback
{
    [callback copy];
    
    __weak typeof(self) weakSelf = self;
    void(^drawImageBlock)(void) = ^{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSArray *rectsArray = [weakSelf photoCellsFramesForCollageWithSize:CGSizeMake(kCollageSideSize, kCollageSideSize)];
            
            CGSize placeholderSize = [[rectsArray firstObject] CGRectValue].size;
            UIImage *placeholderImage = [TCRenderingHelper placeholderImageForSize:placeholderSize];
            
            UIGraphicsBeginImageContext(CGSizeMake(kCollageSideSize, kCollageSideSize));
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
            CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
            CGContextFillRect(context, CGRectMake(0, 0, kCollageSideSize, kCollageSideSize));
            
            for (NSInteger i = 0; i < 4; i++) {
                CGRect rect = [rectsArray[i] CGRectValue];
                TCPhotoObject *photo = [weakSelf photoAtIndex:i];
                UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:photo.standardResolution];
                if (image) {
                    [image drawInRect:rect];
                } else {
                    [placeholderImage drawInRect:rect];
                }
            }
            
            UIImage *resultPhoto = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            dispatch_async(dispatch_get_main_queue(), ^{
                callback(resultPhoto);
            });
        });
    };
    
    NSArray *photoURLs = [[self allPhotos] valueForKey:@"standardResolution"];
    [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:photoURLs progress:nil completed:^(NSUInteger downloaded, NSUInteger skipped)
     {
         drawImageBlock();
     }];
}

@end
