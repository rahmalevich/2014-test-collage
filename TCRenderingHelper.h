//
//  TCRenderingHelper.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 14.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

@interface TCRenderingHelper : NSObject

+ (UIImage *)placeholderImageForSize:(CGSize)size;

@end
