//
//  TCBlocks.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 11.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

typedef void(^TCNetworkResponseBlock)(id response, NSError *error);
typedef void(^TCVoidBlock)(void);
typedef void(^TCDataBlock)(id data);
