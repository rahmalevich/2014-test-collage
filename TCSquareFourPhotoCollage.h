//
//  TCSquareFourPhotoCollage.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 11.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCCollage.h"

@interface TCSquareFourPhotoCollage : TCCollage

- (NSInteger)photoCellsCount;
- (NSArray *)photoCellsFramesForCollageWithSize:(CGSize)size;

@end
