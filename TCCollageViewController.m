//
//  TCCollageViewController.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 04.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCCollageViewController.h"
#import "TCCollageView.h"
#import "TCPickerViewController.h"
#import "TCDataController.h"
#import "TCSquareFourPhotoCollage.h"
#import "MBProgressHUD.h"

#import <MessageUI/MessageUI.h>

@interface TCCollageViewController () <TCDataControllerDelegate, MFMailComposeViewControllerDelegate, TCCollageViewDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) TCDataController *dataController;
@property (nonatomic, strong) TCSquareFourPhotoCollage *collage;
@property (nonatomic, strong) TCCollageView *collageView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (nonatomic, weak) IBOutlet UIView *collageContainerView;
@end

@implementation TCCollageViewController

#pragma mark - Initialization
- (instancetype)initWithUsername:(NSString *)username
{
    if (self = [super init]) {
        self.dataController = [[TCDataController alloc] initWithUsername:username delegate:self];
        self.collage = [[TCSquareFourPhotoCollage alloc] initWithPhotos:@[]];
    }
    return self;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Коллаж";
    _sendButton.hidden = YES;
    [_dataController loadPhotos];
}

#pragma mark - Actions
- (IBAction)actionSend:(UIButton *)sender
{
    if ([MFMailComposeViewController canSendMail]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [_collage saveToImageWithCallback:^(UIImage *photo) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
            MFMailComposeViewController *composerController = [[MFMailComposeViewController alloc] init];
            [composerController addAttachmentData:UIImageJPEGRepresentation(photo, 1.0) mimeType:@"image/jpeg" fileName:@"collage.jpeg"];
            [composerController setSubject:@"Коллаж из тестового задания"];
            composerController.mailComposeDelegate = self;
            [self presentViewController:composerController animated:YES completion:nil];
        }];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Учетная запись mail не настроена" delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - TCDataController delegate
- (void)dataControllerWillStartLoading:(TCDataController *)controller
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)dataControllerDidFinishLoading:(TCDataController *)controller
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    NSArray *photosToSet = [controller.photos count] > 4 ? [controller.photos subarrayWithRange:NSMakeRange(0, 4)] : controller.photos;
    for (NSInteger i = 0; i < 4; i++) {
        TCPhotoObject *photo = photosToSet[i];
        [_collage setPhoto:photo atIndex:i];
    }
    
    self.collageView = [[TCCollageView alloc] initWithFrame:_collageContainerView.bounds collage:_collage delegate:self];
    [_collageContainerView addSubview:_collageView];

    _sendButton.hidden = NO;
}

- (void)dataController:(TCDataController *)controller loadingDidFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Не удалось загрузить фотографии" delegate:self cancelButtonTitle:@"ОК" otherButtonTitles:nil];
    [alert show];
    
}

#pragma mark - MFMailComposeViewController delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TCCollageView delegate
- (void)didTapPhotoWithIndex:(NSInteger)index atCollageView:(TCCollageView *)collageView
{
    NSMutableArray *mutableSelectedPhotos = [[_collage allPhotos] mutableCopy];
    TCPhotoObject *currentPhoto = [_collage photoAtIndex:index];
    [mutableSelectedPhotos removeObject:currentPhoto];
    NSArray *selectedPhotos = [mutableSelectedPhotos copy];

    TCPickerViewController *pickerController = [[TCPickerViewController alloc] initWithDataController:_dataController selectedPhotos:selectedPhotos callback:^(TCPhotoObject *photo)
    {
        [_collage setPhoto:photo atIndex:index];
        [_collageView updatePhotos];
    }];
    [self.navigationController presentViewController:pickerController animated:YES completion:nil];
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
