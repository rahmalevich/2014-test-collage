//
//  TCSquareFourPhotoCollage.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 11.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCSquareFourPhotoCollage.h"

@interface TCSquareFourPhotoCollage ()
@property (nonatomic, strong) NSDictionary *placeholdersDict;
@end

@implementation TCSquareFourPhotoCollage

#pragma mark - Overriden methods
- (NSInteger)photoCellsCount
{
    return 4;
}

- (NSArray *)photoCellsFramesForCollageWithSize:(CGSize)size
{
    CGFloat side = MIN(size.width, size.height);
    CGFloat padding = ceilf(side / 100.0);
    CGFloat photoWidth = floorf((side - padding * 3) / 2);
    CGFloat secondPhotoOffset = side - padding - photoWidth;

    CGFloat xOffset = side < size.width ? floorf((size.width - side)/2) : 0.0;
    CGFloat yOffset = side < size.height ? floorf((size.height - side)/2) : 0.0;
    
    CGRect rect1 = CGRectMake(xOffset + padding, yOffset + padding, photoWidth, photoWidth);
    CGRect rect2 = CGRectMake(xOffset + secondPhotoOffset, yOffset + padding, photoWidth, photoWidth);
    CGRect rect3 = CGRectMake(xOffset + padding, yOffset + secondPhotoOffset, photoWidth, photoWidth);
    CGRect rect4 = CGRectMake(xOffset + secondPhotoOffset, yOffset + secondPhotoOffset, photoWidth, photoWidth);
    NSArray *rectsArray = @[[NSValue valueWithCGRect:rect1], [NSValue valueWithCGRect:rect2], [NSValue valueWithCGRect:rect3], [NSValue valueWithCGRect:rect4]];

    return rectsArray;
}

@end
