//
//  TCCollageView.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 11.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

@class TCCollageView, TCPhotoObject, TCCollage;
@protocol TCCollageViewDelegate <NSObject>
- (void)didTapPhotoWithIndex:(NSInteger)index atCollageView:(TCCollageView *)collageView;
@end

@interface TCCollageView : UIView

- (instancetype)initWithFrame:(CGRect)frame collage:(TCCollage *)collage delegate:(id<TCCollageViewDelegate>)delegate;
- (void)updatePhotos;

@end
