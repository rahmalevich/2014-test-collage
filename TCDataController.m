//
//  TCDataController.m
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 09.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

#import "TCDataController.h"
#import "TCNetworkHelper.h"
#import "TCPhotoObject.h"

@interface TCDataController ()
@property (nonatomic, copy, readwrite) NSString *username;
@property (nonatomic, weak) id<TCDataControllerDelegate> delegate;
@property (nonatomic, strong, readwrite) NSArray *photos;
@property (nonatomic, strong) NSMutableArray *rawPhotosArray;
@end

@implementation TCDataController

#pragma mark - Initialization
- (instancetype)initWithUsername:(NSString *)username delegate:(id<TCDataControllerDelegate>)delegate
{
    if (self = [super init]) {
        self.username = username;
        self.delegate = delegate;
    }
    return self;
}

#pragma mark - Public
// 1. Ищем пользователя по нику
//    Если не нашли - возвращаем ошибку
// 2. Если нашли - в цикле загружаем информацию о фотографиях
// 3. Ищем 10 фотографий с максимальным количеством лайков и сохраняем информацию о них в памяти контроллера
- (void)loadPhotos
{
    [_delegate dataControllerWillStartLoading:self];
    
    self.rawPhotosArray = [NSMutableArray array];
    [self step1_getIdForUser];
}

#pragma mark - Private
- (void)step1_getIdForUser
{
    __weak typeof(self) weakSelf = self;
    [[TCNetworkHelper sharedInstance] loadRequestWithPath:@"/v1/users/search" params:@{@"q": _username} callback:^(NSDictionary *response, NSError *error)
     {
         if (!error) {
             NSString *userId = nil;
             if ([response isKindOfClass:[NSDictionary class]]) {
                 NSArray *usersArray = response[@"data"];
                 if ([usersArray count] > 0) {
                     NSDictionary *userDict = [usersArray firstObject];
                     userId = userDict[@"id"];
                 }
             }
             
             if ([userId length] > 0) {
                 [weakSelf step2_loadPhotosForId:userId orURL:nil];
             } else {
                 [_delegate dataController:weakSelf loadingDidFailWithError:[NSError errorWithDomain:kTCErrorDomain code:TCErrorCodeUserNotFound userInfo:nil]];
             }
         } else {
             [_delegate dataController:weakSelf loadingDidFailWithError:error];
         }
     }];
}

- (void)step2_loadPhotosForId:(NSString *)userId orURL:(NSString *)urlString
{
    NSString *path = urlString ?: [NSString stringWithFormat:@"/v1/users/%@/media/recent", userId];
    [[TCNetworkHelper sharedInstance] loadRequestWithPath:path params:nil callback:^(NSDictionary *response, NSError *error)
     {
         if (!error) {
             NSString *nextUrl = nil;
             if ([response isKindOfClass:[NSDictionary class]]) {
                 nextUrl = [response valueForKeyPath:@"pagination.next_url"];
                 NSArray *loadedPhotos = response[@"data"];
                 if ([loadedPhotos isKindOfClass:[NSArray class]]) {
                     [self.rawPhotosArray addObjectsFromArray:loadedPhotos];
                 }
             }
             
             if ([nextUrl length] > 0) {
                 [self step2_loadPhotosForId:nil orURL:nextUrl];
             } else {
                 [self step3_processRawPhotos:self.rawPhotosArray];
             }
         } else {
             if ([self.rawPhotosArray count] > 0) {
                 [self step3_processRawPhotos:self.rawPhotosArray];
             } else {
                 [_delegate dataController:self loadingDidFailWithError:error];
             }
         }
     }];
}

- (void)step3_processRawPhotos:(NSArray *)array
{
    NSArray *photos = [TCPhotoObject photosFromArray:array];
    photos = [photos sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"likesCount" ascending:NO]]];
    if ([photos count] > kBestPhotosCount) {
        photos = [photos subarrayWithRange:NSMakeRange(0, kBestPhotosCount)];
    }
    self.photos = photos;
    
    [_delegate dataControllerDidFinishLoading:self];
}

@end
