//
//  TCCollage.h
//  test-collage
//
//  Created by Mikhail Rakhmalevich on 11.07.14.
//  Copyright (c) 2014 Mikhail Rahmalevich. All rights reserved.
//

@class TCPhotoObject;
@interface TCCollage : NSObject

- (instancetype)initWithPhotos:(NSArray *)photos;

- (NSInteger)photoCellsCount;
- (NSArray *)photoCellsFramesForCollageWithSize:(CGSize)size;

- (NSArray *)allPhotos;
- (TCPhotoObject *)photoAtIndex:(NSInteger)index;
- (void)setPhoto:(TCPhotoObject *)photo atIndex:(NSInteger)index;
- (void)saveToImageWithCallback:(TCDataBlock)callback;

@end
